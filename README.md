**SpeechIO asset for the RAGE project**

This asset features components for natural language processing, suitable to be used in game development. It is an implementation of clients - one for each component - that communicate with their server counterparts - one for each component.

It includes: a) VS2015 C# project for SpeechIO, b) the employed RAGE AssetManager, c) a demo in Unity 5.3.5 (full project) for the necessary steps to access the various components of the SpeechIO asset