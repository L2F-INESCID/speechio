﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AssetPackage;
using AssetManagerPackage;
using System;

public class NewBehaviourScript : MonoBehaviour {

	// Use this for initialization
	IEnumerator Start () {
        Bridge mybridge = new Bridge();
        AssetManager.Instance.Bridge = mybridge;

        SpeechIO asset = new SpeechIO();
        asset.Bridge = mybridge;

        // TTS
        List<TTSresult> SynthResults = asset.TextToSpeech("Eu comi uma maçã");
        WWW b = new WWW(SynthResults[0].audioURL);
        yield return b;
        GetComponent<AudioSource>().PlayOneShot(b.GetAudioClip());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
