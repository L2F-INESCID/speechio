**Demo of the SpeechIO asset for the RAGE project**

This asset is a Unity 5.3.5 project which details necessary steps to access the various components of the SpeechIO asset. 
It is not yet using the RAGE compliant SpeechIO asset. Instead, it is an implementation of REST clients - one for each component - that communicate with their server counterparts - one for each component.