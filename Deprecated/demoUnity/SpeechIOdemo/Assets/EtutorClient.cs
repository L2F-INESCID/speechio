using UnityEngine;
using System.Collections;
using System.ComponentModel;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using System.IO;
using System.Linq;
using System.Xml;
using System.Text;
using WWUtils.Audio;

public class EtutorClient : MonoBehaviour
{
    public GUISkin Skin;
    public enum requrls
    {
        [Description("http://localhost:13001")]
        localhost,
        [Description("frameworkProxy.php")]
        proxy,
        [Description("www.l2f.inesc-id.pt/~pfialho/test/echo2/frameworkProxy22.py")]
        test
    };

    //www.l2f.inesc-id.pt/~pfialho/test/frameworkProxy2.py

    public requrls requrl = requrls.test;
    public string audioDLpreffix = "";

    private int micSampleRate = 0;
    public float micKeepRecAfterStopSec = 1;
    bool hasError = false;
    bool inRec = false;
    bool startRec = false;
    bool stopRec = false;
    byte[] signedBytes = new byte[0];
    float[] signedFloats = new float[0];

    private string ReqURL
    {
        get
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                requrl = requrls.test;              //force remote
                return "http://" + ((DescriptionAttribute)requrl.GetType().GetField(requrl.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false)[0]).Description;
            }
            else
            {
                return ((DescriptionAttribute)requrl.GetType().GetField(requrl.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false)[0]).Description;
            }
        }
    }
    private string AudioDLpreffix
    {
        get
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                return "http://www.l2f.inesc-id.pt/~pfialho/test/";     //force remote
            }
            else
            {
                if (requrl == requrls.test && (!Application.isWebPlayer || Application.isEditor))
                {
                    return "www.l2f.inesc-id.pt/~pfialho/test/echo2/";
                }
                else
                    return audioDLpreffix;
            }
        }
    }

    private List<EmotionInfo> emotionResults = null;
    internal List<EmotionInfo> EmotionResults { get { return emotionResults; } private set { emotionResults = value; } }
    private List<SynthInfo> synthResults = null;
    internal List<SynthInfo> SynthResults { get { return synthResults; } private set { synthResults = value; } }
    private bool crossdomainOK = true;
    private bool lipsyncOK = false;
    private bool working = false;
    private bool wwwError = false;
    private string wwwErrorMsg = "";
    internal string WWWerrorMsg { get { return wwwErrorMsg; } private set { wwwErrorMsg = value; } }
    internal bool WWWerror { get { return wwwError; } private set { wwwError = value; } }
    internal bool CrossdomainOK { get { return crossdomainOK; } private set { crossdomainOK = value; } }
    internal bool LipsyncOK { get { return lipsyncOK; } private set { lipsyncOK = value; } }
    internal bool Working { get { return working; } set { working = value; } }      //private set { working = value; } }
    internal enum voices { roger_hts2010, slt_hts2010, cstr_uk_nina_3hour_hts, clb_hts2010, uvigo3_hts2010, gth_sev_rosa_neutral1011_hts, Vicente, Violeta };
    internal voices voice = voices.roger_hts2010;
    internal Dictionary<string, string> currPhonvis = LangResources.phonvisEN;
    internal string[] currVis = LangResources.visEN;

    public Dropdown asrtasksel;
    public Dropdown asrlangsel;
    public Dropdown ttsvoices;

    bool recdone = false;

    IEnumerator Start()
    {
        yield return Application.RequestUserAuthorization(UserAuthorization.Microphone);

        //    Debug.Log("selected: " + tasksel.value);
        //    Debug.Log("selected: " + langsel.value);

        //    tasksel.onValueChanged.AddListener(delegate
        //    {
        //        myDropdownValueChangedHandler(tasksel);
        //    });

        //    langsel.onValueChanged.AddListener(delegate
        //    {
        //        myDropdownValueChangedHandler(langsel);
        //    });
    }

    //void Destroy()
    //{
    //    tasksel.onValueChanged.RemoveAllListeners();
    //    langsel.onValueChanged.RemoveAllListeners();
    //}

    //private void myDropdownValueChangedHandler(Dropdown target)
    //{
    //    Debug.Log("selected: " + target.value);
    //}

    //public void SetDropdownIndex(int index)
    //{
    //    tasksel.value = index;
    //}

    string[] ttsvoicesopts = new string[3] { "Vicente", "Viriato", "Violeta" };

    IEnumerator SendTextToServer(string tts)
    {
        Debug.Log(tts);
        WWWForm form = new WWWForm();
        form.AddField("task", "echo");
        form.AddField("lang", "pt");
        form.AddField("v", ttsvoicesopts[ttsvoices.value]);
        form.AddField("q", tts);

        string prefx = "";
        if (!Application.isWebPlayer || Application.isEditor)
        {
            prefx = "www.l2f.inesc-id.pt/~pfialho/tts/";
        }

        WWW www = new WWW(prefx + "frameworkProxy.php", form);
        yield return www;
        if (www.error != null)
        {
            WWWerrorMsg = www.error;
            WWWerror = true;
            yield break;
        }

        yield return StartCoroutine("parseJSONResponse", www.text);

        string toprint = "";
        foreach (SynthInfo res in SynthResults)
        {
            toprint += res.sentence + "\n\t"
                + String.Join(", ", res.phon.ToArray(), 0, Math.Min(res.phon.Count - 1, 10)) + ", ...\n\t"
                + string.Join(", ", res.phontimes.Select(f => f.ToString()).ToArray(), 0, Math.Min(res.phontimes.Count - 1, 10)) + ", ...\n\n";
        }

        GameObject.Find("/Canvas/tts/log").GetComponent<Text>().text = toprint;
        GameObject.Find("/Canvas/tts/playspeech").GetComponent<Button>().interactable = true;
        GameObject.Find("/Canvas/tts/getspeech").GetComponent<Button>().interactable = true;
    }

    IEnumerator parseJSONResponse(string jsontext)
    {
        print("www: " + jsontext);

        Hashtable json2 = (Hashtable)MiniJSON.JsonDecode(jsontext);

        ArrayList alltts = (ArrayList)json2["ttsresults"];
        SynthResults = new List<SynthInfo>();
        foreach (Hashtable json in alltts)
        {
            List<string> phon = new List<string>();
            phon = new List<string>(((ArrayList)json["phones"]).ToArray(typeof(string)) as string[]);

            //lite invalid/empty result checking!...
            if (phon.Count <= 0)
            {
                continue;
            }

            //print("phones: " + string.Join(",", SynthInfo.phon.ToArray(), 0, 5));
            //List<string> timestoprint = new List<string>();

            List<float> phontimes = new List<float>();
            ArrayList tmp = (ArrayList)json["times"];
            foreach (double f in tmp)
            {
                phontimes.Add((float)f);
                //timestoprint.Add(f.ToString());
            }
            //print("times: " + string.Join(",", timestoprint.ToArray(), 0, 5));

            string sentence = (string)json["sentence"];
            sentence = WWW.UnEscapeURL(sentence);
            print("sentence: " + sentence);

            //SynthInfo.emglobal = "";
            //emot = (double)json["eint"];

            string oggurl = AudioDLpreffix + (string)json["url"];
            if (oggurl.Trim().Length == 0)
            {
                yield break;
            }

            print("audio url: " + oggurl);

            WWW ogg = new WWW(oggurl);
            yield return ogg;
            AudioClip audioOut = ogg.audioClip;

            SynthResults.Add(new SynthInfo(sentence, phon, phontimes, audioOut));
        }

        ArrayList allemotions = (ArrayList)json2["ted"];
        EmotionResults = new List<EmotionInfo>();
        foreach (Hashtable f in allemotions)
        {
            string text = (string)f["text"];
            string emotion = (string)f["emotion"];
            double duration = (double)f["duration"];

            EmotionResults.Add(new EmotionInfo(WWW.UnEscapeURL(text), emotion.ToLower().Trim(), (float)duration));
        }
    }

    string asrout = "";
    string[] taskopts = new string[2] { "echo-asr", "echo-kws-asr" };
    string[] langopts = new string[3] { "fr", "en", "pt" };

    IEnumerator SendAudioToServer(byte[] sbytes)
    {
        WWWForm form = new WWWForm();
        form.AddField("task", taskopts[asrtasksel.value]);
        form.AddField("lang", langopts[asrlangsel.value]);

        if (asrtasksel.value == 1)
        {
            string grxml = GameObject.Find("/Canvas/asr/InputField/Text").GetComponent<Text>().text;
            if (grxml.Length == 0)
            {
                if (langopts[asrlangsel.value] == "fr")
                {
                    grxml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><grammar root=\"main\" version=\"1.0\" xmlns=\"http://www.w3.org/2001/06/grammar\" xml:lang=\"fr-FR\"><rule id=\"main\"><item repeat=\"1-\"><one-of>";
                    string[] keys = GameObject.Find("/Canvas/asr/inputKeys/Text").GetComponent<Text>().text.Split('\n');
                    for (int i = 0; i < keys.Length; i++)
                    {
                        grxml += "<item>" + keys[i] + "</item>";
                    }

                    grxml += "<item><ruleref uri=\"#garbage\"/></item></one-of></item></rule><rule id=\"garbage\"><item repeat=\"1-\"><item>background</item></item></rule></grammar>";
                }
                else
                {
                    grxml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><grammar root=\"main\" version=\"1.0\" xmlns=\"http://www.w3.org/2001/06/grammar\" xml:lang=\"pt-PT\"><rule id=\"main\"><item repeat=\"0-1\"><one-of>";
                    string[] keys = GameObject.Find("/Canvas/asr/inputKeys/Text").GetComponent<Text>().text.Split('\n');
                    for (int i = 0; i < keys.Length; i++)
                    {
                        grxml += "<item><ruleref special=\"GARBAGE\"/>" + keys[i] + "<ruleref special=\"GARBAGE\"/></item>";
                    }

                    grxml += "</one-of></item></rule></grammar>";
                }
            }

            Debug.Log(grxml);
            form.AddField("grxml", grxml);
        }

        //if (requrl.Equals(requrls.test))
        //{
        //    form.AddBinaryData("sbytes", sbytes);
        //}
        //else
        //{
        Byte[] wavFileInByteArray = sbytes;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append("[");
        int count = 0;
        for (int i = 0; i < wavFileInByteArray.Length; i++)
        {
            if (i == wavFileInByteArray.Length - 1)
            {
                sb.Append(wavFileInByteArray[i]);
            }
            else
            {
                sb.Append(wavFileInByteArray[i] + ",");
            }
            if (wavFileInByteArray[i] != 0)
                count++;
        }
        sb.Append("]");

        form.AddField("sbytes", sb.ToString());
        //}

        //string requ = ReqURL;
        string requ = "frameworkProxy22.py";
        if (!Application.isWebPlayer || Application.isEditor)
        {
            requ = "www.l2f.inesc-id.pt/~pfialho/test/echo2/frameworkProxy22.py";
        }

        if (langopts[asrlangsel.value] == "fr")
        {
            requ = "frameworkProxyFR.py";
            if (!Application.isWebPlayer || Application.isEditor)
            {
                requ = "www.l2f.inesc-id.pt/~pfialho/test/echo2/frameworkProxyFR.py";
            }
        }

        /*
        if (taskopts[asrtasksel.value] == "echo-asr" && (langopts[asrlangsel.value] == "pt" || langopts[asrlangsel.value] == "en"))
        {
            Debug.Log("using KALDI");
            string prefx = "";
            if (!Application.isWebPlayer || Application.isEditor)
            {
                prefx = "www.l2f.inesc-id.pt/~pfialho/test/echo2/";
            }
            requ = prefx + "frameworkProxyKaldi.php"; //Kaldi.php
        }
        */

        WWW www = new WWW(requ, form);
        yield return www;
        GameObject.Find("/Canvas/asr/gettext").GetComponent<Button>().interactable = true;
        if (www.error != null)
        {
            Debug.Log(www.error);
            WWWerrorMsg = www.error;
            WWWerror = true;
            yield break;
        }

        asrout = www.text;
        Debug.Log("output text: " + asrout);

        if (asrout.Trim() == "_REPEAT_" || (langopts[asrlangsel.value] == "fr" && asrout.Replace("background", "").Trim().Length == 0))
        {
            GameObject.Find("/Canvas/asr/Text").GetComponent<Text>().text = "<keyword not found in speech>";
        }
        else
        {
            if (langopts[asrlangsel.value] == "fr" && asrtasksel.value == 1)
            {
                GameObject.Find("/Canvas/asr/Text").GetComponent<Text>().text = asrout.Replace("background", " ");
            }
            else
            {
                GameObject.Find("/Canvas/asr/Text").GetComponent<Text>().text = asrout;
            }
        }

        //yield return StartCoroutine("parseJSONResponse", www.text);
    }

    void stop()
    {
        StopAllCoroutines();
        SynthResults = null;
        EmotionResults = null;
        WWWerror = false;
        Working = false;
        LipsyncOK = false;
        signedBytes = new byte[0];
        signedFloats = new float[0];
        GetComponent<AudioSource>().Stop();
    }

    public void startrec()
    {
        micSampleRate = 16000;
        SendMessage("stop");
        stop();
        GameObject.Find("/Canvas/asr/Play").GetComponent<Button>().interactable = false;
        GameObject.Find("/Canvas/asr/gettext").GetComponent<Button>().interactable = false;
        Microphone.End(null);
        StopCoroutine("Record");
        recdone = false;

        StartCoroutine("Record");
    }

    IEnumerator Record()
    {
        AudioClip recording = Microphone.Start(null, false, 60, micSampleRate);
        //while (!(Microphone.GetPosition(null) > 0)) { }

        int endsample = 0;
        stopRec = false;
        while (!stopRec)        //Microphone.IsRecording(null)
        {
            endsample = Microphone.GetPosition(null);
            yield return new WaitForSeconds(0.1f);
        }
        if (startRec)       //to ensure
        {
            startRec = false;
        }

        print("endsample1: " + endsample);
        //print("rec samples: " + recording.samples);

        if (endsample < (micSampleRate / 4))
        {
            yield break;
        }

        Working = true;
        yield return new WaitForSeconds(micKeepRecAfterStopSec);
        endsample = Microphone.GetPosition(null);
        Microphone.End(null);

        print("endsample2: " + endsample);

        float[] recordingFloatArray = new float[endsample];     // * stereo
        recording.GetData(recordingFloatArray, 0);

        signedFloats = recordingFloatArray;
        byte[] soundBytes = new byte[recordingFloatArray.Length * 2];
        for (int i = 0; i < recordingFloatArray.Length; i++)
        {
            byte[] aux = System.BitConverter.GetBytes((short)(recordingFloatArray[i] * 32767));
            soundBytes[i * 2] = (byte)aux[0];
            soundBytes[i * 2 + 1] = (byte)aux[1];
        }

        signedBytes = soundBytes;
        Debug.Log("signedBytes1: " + signedBytes.Length);
        recdone = true;
    }

    public void stoprec()
    {
        StartCoroutine("stoprec2", "asr");
    }

    IEnumerator stoprec2(string canvasname)
    {
        stopRec = true;

        while (!recdone)
        {
            yield return null;
        }

        Debug.Log("signedBytes2: " + signedBytes.Length);
        if (signedBytes.Length > 0)
        {
            GameObject.Find("/Canvas/" + canvasname + "/Play").GetComponent<Button>().interactable = true;
            GameObject.Find("/Canvas/" + canvasname + "/gettext").GetComponent<Button>().interactable = true;
        }
    }

    public void gettext()
    {
        GameObject.Find("/Canvas/asr/Text").GetComponent<Text>().text = "";
        GameObject.Find("/Canvas/asr/gettext").GetComponent<Button>().interactable = false;
        StartCoroutine("SendAudioToServer", signedBytes);
    }

    public void getspeech()
    {
        SendMessage("stop");
        stop();
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        GameObject.Find("/Canvas/tts/getspeech").GetComponent<Button>().interactable = false;
        GameObject.Find("/Canvas/tts/playspeech").GetComponent<Button>().interactable = false;
        GameObject.Find("/Canvas/tts/stopplay").GetComponent<Button>().interactable = false;
        GameObject.Find("/Canvas/tts/log").GetComponent<Text>().text = "";

        StartCoroutine("SendTextToServer", GameObject.Find("/Canvas/tts/InputField/Text").GetComponent<Text>().text);
    }

    public void stopplayspeech()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        GameObject.Find("/Canvas/tts/playspeech").GetComponent<Button>().interactable = true;
        GameObject.Find("/Canvas/tts/stopplay").GetComponent<Button>().interactable = false;
        StopCoroutine("playrawtts2");
    }

    public void playraw()
    {
        StartCoroutine("playraw2");
    }

    IEnumerator playraw2()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.Stop();
        //StopAllCoroutines();
        Debug.Log("signedBytes3: " + signedBytes.Length);

        if (signedBytes.Length > 0)
        {
            //set audio to play upon request

            AudioClip audioClip = AudioClip.Create("testSound", signedFloats.Length, 1, micSampleRate, false);
            audioClip.SetData(signedFloats, 0);

            while (audioClip.loadState != AudioDataLoadState.Loaded)
            {
                yield return new WaitForSeconds(0.5f);
            }
            //audio.clip = audioClip;
            //audio.Play();
            audio.PlayOneShot(audioClip);
        }
    }

    public void playrawtts()
    {
        GameObject.Find("/Canvas/tts/playspeech").GetComponent<Button>().interactable = false;
        GameObject.Find("/Canvas/tts/stopplay").GetComponent<Button>().interactable = true;
        StartCoroutine("playrawtts2");
    }

    IEnumerator playrawtts2()
    {
        AudioSource audio = GetComponent<AudioSource>();

        float waitmore = 0f;
        if (SynthResults.Count > 1)
        {
            waitmore = 1f;
        }

        foreach (SynthInfo res in SynthResults)
        {
            audio.clip = res.audioOut;
            //audio.PlayOneShot(res.audioOut);
            audio.Play();
            yield return new WaitForSeconds(res.audioOut.length + waitmore);
        }

        GameObject.Find("/Canvas/tts/playspeech").GetComponent<Button>().interactable = true;
        GameObject.Find("/Canvas/tts/stopplay").GetComponent<Button>().interactable = false;
    }

    public void loadexASR()
    {
        //String xml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><grammar root=\"main\" version=\"1.0\" xmlns=\"http://www.w3.org/2001/06/grammar\" xml:lang=\"pt-PT\"><rule id=\"main\"><item repeat=\"0-1\"><one-of><item><ruleref special=\"GARBAGE\"/>Portugal<ruleref special=\"GARBAGE\"/></item></one-of></item></rule></grammar>";
        //string newtext = PrintXML(xml);
        GameObject.Find("/Canvas/asr/inputKeys").GetComponent<InputField>().text = "hello\nworld";
        asrtasksel.value = 1;
        asrlangsel.value = 1;
    }

    // Emotion from speech
    public void startrecEmosp()
    {
        micSampleRate = 8000;
        SendMessage("stop");
        stop();
        GameObject.Find("/Canvas/emosp/Play").GetComponent<Button>().interactable = false;
        Microphone.End(null);
        StopCoroutine("Record");
        recdone = false;

        StartCoroutine("Record");
    }

    public void stoprecEmosp()
    {
        StartCoroutine("stoprec2", "emosp");
    }

    public void getEmosp()
    {
        GameObject.Find("/Canvas/emosp/log").GetComponent<Text>().text = "";
        GameObject.Find("/Canvas/emosp/gettext").GetComponent<Button>().interactable = false;
        StartCoroutine(SendToSPA("emospeechProxy.php", "emosp"));
    }

    // Dialog act and Emotion from text
    public void loadexDAct()
    {
        GameObject.Find("/Canvas/sact/InputField").GetComponent<InputField>().text = "Hello!\r\nHi, how are you?\r\nI'm awesome!\r\nDo you think I am amazing?\r\nNo.\r\nWhy do you say that?\r\nBecause I am the best!\r\nOkay, sure.";
    }

    public void getdialogact()
    {
        GameObject.Find("/Canvas/sact/log").GetComponent<Text>().text = "";
        GameObject.Find("/Canvas/sact/getspeech").GetComponent<Button>().interactable = false;
        StartCoroutine(SendToSPA("dialogActProxy.php", "sact"));
    }

    public void loadexEmot()
    {
        GameObject.Find("/Canvas/emot/InputField").GetComponent<InputField>().text = "Mortar assault leaves at least 18 dead\r\nGoal delight for Sheva\r\nNigeria hostage feared dead is freed\r\nBombers kill shoppers\r\nVegetables, not fruit, slow brain decline\r\nPM: Havana deal a good experiment\r\nNASA revisiting life on Mars question\r\nHappy birthday, iPod\r\nAlonso would be happy to retire with three titles\r\nMadonna's New Tot 'Happy at Home' in London\r\nNicole Kidman asks dad to help stop husband's drinking\r\nWe were 'arrogant and stupid' over Iraq, says US diplomat\r\nBad reasons to be good";
    }

    public void getemotext()
    {
        GameObject.Find("/Canvas/emot/log").GetComponent<Text>().text = "";
        GameObject.Find("/Canvas/emot/getspeech").GetComponent<Button>().interactable = false;
        StartCoroutine(SendToSPA("emotextProxy.php", "emot"));
    }

    public IEnumerator SendToSPA(string selphp, string canvasname)
    {
        string tosend = "";
        if (!(canvasname == "emosp"))
        {
            tosend = GameObject.Find("/Canvas/" + canvasname + "/InputField").GetComponent<InputField>().text;
            tosend += "\r\n";
            tosend = "<root><text><s>" + tosend.Replace("\r\n", "</s><s>");
            tosend = tosend.Substring(0, tosend.Length - 3) + "</text></root>";
            tosend = tosend.Replace("<s></s>", "");
            Debug.Log(tosend);
        }
        else
        {
            #region wavheader
            byte[] wavheader = new byte[44];
            Byte[] riff = System.Text.Encoding.UTF8.GetBytes("RIFF");
            //fileStream.Write(riff, 0, 4);
            Array.Copy(riff, 0, wavheader, 0, 4);

            Byte[] chunkSize = BitConverter.GetBytes(signedBytes.Length - 8);
            //fileStream.Write(chunkSize, 0, 4);
            Array.Copy(chunkSize, 0, wavheader, 4, 4);

            Byte[] wave = System.Text.Encoding.UTF8.GetBytes("WAVE");
            Array.Copy(wave, 0, wavheader, 8, 4);

            Byte[] fmt = System.Text.Encoding.UTF8.GetBytes("fmt ");
            Array.Copy(fmt, 0, wavheader, 12, 4);

            Byte[] subChunk1 = BitConverter.GetBytes(16);
            Array.Copy(subChunk1, 0, wavheader, 16, 4);

            UInt16 two = 2;
            UInt16 one = 1;

            Byte[] audioFormat = BitConverter.GetBytes(one);
            Array.Copy(audioFormat, 0, wavheader, 20, 2);

            Byte[] numChannels = BitConverter.GetBytes(1);
            Array.Copy(numChannels, 0, wavheader, 22, 2);

            Byte[] sampleRate = BitConverter.GetBytes(8000);
            Array.Copy(sampleRate, 0, wavheader, 24, 4);

            Byte[] byteRate = BitConverter.GetBytes(16000); // sampleRate * bytesPerSample*number of channels, here 44100*2*2
            Array.Copy(byteRate, 0, wavheader, 28, 4);

            UInt16 blockAlign = (ushort)(2);
            Array.Copy(BitConverter.GetBytes(blockAlign), 0, wavheader, 32, 2);

            UInt16 bps = 16;
            Byte[] bitsPerSample = BitConverter.GetBytes(bps);
            Array.Copy(bitsPerSample, 0, wavheader, 34, 2);

            Byte[] datastring = System.Text.Encoding.UTF8.GetBytes("data");
            Array.Copy(datastring, 0, wavheader, 36, 4);

            Byte[] subChunk2 = BitConverter.GetBytes(signedBytes.Length * 2);
            Array.Copy(subChunk2, 0, wavheader, 40, 4);

            byte[] wavarr = new byte[wavheader.Length + signedBytes.Length];
            wavheader.CopyTo(wavarr, 0);
            signedBytes.CopyTo(wavarr, wavheader.Length);

            // debug wav
            WAV wav = new WAV(wavarr);
            Debug.Log(wav);
            AudioClip audioClip = AudioClip.Create("testSound", wav.SampleCount, 1, wav.Frequency, false, false);
            audioClip.SetData(wav.LeftChannel, 0);
            //AudioSource audio = GetComponent<AudioSource>();
            //audio.clip = audioClip;
            //audio.Play();
            //SavWav.Save("test", audioClip);            

            tosend = "<root><audio>" + System.Convert.ToBase64String(wavarr) + "</audio></root>";
            Debug.Log(tosend.Substring(0, 50) + "..." + tosend.Substring(tosend.Length - 40, 40));
            #endregion
        }

        Dictionary<string, string> headers = new Dictionary<string, string>();

        headers["Content-Type"] = "application/xml";

        //Debug.Log(headers["Authorization"]);

        string prefx = "";
        if (!Application.isWebPlayer || Application.isEditor)
        {
            prefx = "www.l2f.inesc-id.pt/~pfialho/test/echo2/";
        }

        WWW www = new WWW(prefx + selphp, System.Text.Encoding.ASCII.GetBytes(tosend), headers);     // https://www.l2f.inesc-id.pt/spa/services/dialogAct
        yield return www;
        if (www.error != null)
        {
            Debug.Log(www.error);
            yield break;
        }

        string outtext = www.text;
        Debug.Log(www.text);

        //if (canvasname == "emosp")
        //{
        //    outtext = outtext.Remove(0, outtext.IndexOf("1:") + 8);
        //}
        //else
        //{                        
        outtext = outtext.Remove(0, outtext.IndexOf("<result>") + 8);
        outtext = outtext.Remove(outtext.IndexOf("</result>"), outtext.Length - outtext.IndexOf("</result>") - 1);
        //}

        if (canvasname == "emot")
        {
            GameObject.Find("/Canvas/emot/header").GetComponent<Text>().text = "anger		surprise		disgust		fear		    joy		sadness";
            string bakO = outtext;
            outtext = "";

            string[] classtyp = bakO.Substring(bakO.IndexOf("<") + 1, bakO.IndexOf("\n")).Split('<');
            bakO = bakO.Remove(0, bakO.IndexOf("\n"));

            string[] lines = bakO.Split('\n');
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Trim().Length <= 0)
                    continue;

                string numline = lines[i].Remove(0, lines[i].IndexOf("<") + 1);
                string[] vals = numline.Split('<');
                outtext += String.Join("       ", vals) + "\n";

                //double[] aLineVals = Array.ConvertAll<string, double>(vals, Double.Parse);
                //List<double> reversed = aLineVals.ToList().Select(x => Math.Abs(x)).ToList();
                //Debug.Log(String.Join(", ", reversed.Select(f => f.ToString()).ToArray()));
                //int maxIndex = reversed.IndexOf(reversed.Max());
                //outtext += classtyp[maxIndex] + "\n";
            }

            outtext = outtext.Trim();
        }

        GameObject.Find("/Canvas/" + canvasname + "/log").GetComponent<Text>().text = outtext;
        if (canvasname == "emosp")
        {
            GameObject.Find("/Canvas/" + canvasname + "/gettext").GetComponent<Button>().interactable = true;
        }
        else
        {
            GameObject.Find("/Canvas/" + canvasname + "/getspeech").GetComponent<Button>().interactable = true;
        }

    }

    // QA
    public void loadexQA()
    {
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><corpus type=\"example\"><qa><questions><q en=\"_WELCOME_\" ></q><q en=\"Hello.\" ></q><q en=\"Hi\" ></q><q en=\"Hi\" ></q><q en=\"Hi\" ></q><q en=\"Hi John! Do you understand me?\"></q><q en=\"Hi, I'm ...!\"></q><q en=\"Hello, I'm ... and you?\"></q></questions><answers><a en=\"Hello, I'm John. What would you like to know about the palace?\" emotion=\"friendly\" intensity=\"100\"></a><a en=\"My name is John and I am at your service to answer any questions you may have about the palace.\" ></a><a en=\"Hello, I'm John. Tell me what you want to know about the palace, it will be my pleasure to help you.\" ></a><a en=\"My name is John and I am at your service to answer any questions about the palace.\" ></a></answers></qa><qa category=\"repeat\"><questions><q en=\"_REPEAT_\" ></q></questions><answers><a en=\"I don't think I understood.\" ></a><a en=\"Could you repeat, please?\" ></a><a en=\"I didn't understand. Could you ask:\"  repeat=\"1\"><fillWith category=\"palace\"/></a><a en=\"I didn't understand. Ask me:\"  repeat=\"1\"><fillWith category=\"palace\"/></a><a en=\"As I still don't understand, I'm going to tell you about the palace.\"  repeat=\"2\"></a><a en=\"The palace's mirror was sold at auction.\"  repeat=\"3\"></a></answers></qa>	<qa category=\"palace\"><questions><q en=\"When did the renovation of the library took place?\" ></q></questions><answers><a en=\"The work took place between May two thousand and eight and February two thousand and nine.\" ></a></answers></qa></corpus>";
        string newtext = PrintXML(xml);
        GameObject.Find("/Canvas/nlu/QAxml").GetComponent<InputField>().text = newtext;

        //content size fitter
        //RectTransform rt = GameObject.Find("/Canvas/nlu/QAxml/a/Text").GetComponent<RectTransform>();
        //Text txt = GameObject.Find("/Canvas/nlu/QAxml/a/Text").GetComponent<Text>();

        //TextGenerator textGen = new TextGenerator();
        //TextGenerationSettings generationSettings = txt.GetGenerationSettings(txt.rectTransform.rect.size);
        //float height = textGen.GetPreferredHeight(newtext, generationSettings);

        //rt.sizeDelta = new Vector2(0, height);

        GameObject.Find("/Canvas/nlu/tname").GetComponent<InputField>().text = "newQA";
        GameObject.Find("/Canvas/nlu/question").GetComponent<InputField>().text = "what happened to the library?";
    }

    public void askQA()
    {
        GameObject.Find("/Canvas/nlu/log").GetComponent<Text>().text = "";
        GameObject.Find("/Canvas/nlu/answer").GetComponent<Text>().text = "";

        StartCoroutine("SendTextToServerQA", GameObject.Find("/Canvas/nlu/question").GetComponent<InputField>().text);
        GameObject.Find("/Canvas/nlu/ask").GetComponent<Button>().interactable = false;
    }

    public void regQA()
    {
        GameObject.Find("/Canvas/nlu/log").GetComponent<Text>().text = "";
        GameObject.Find("/Canvas/nlu/answer").GetComponent<Text>().text = "";
        GameObject.Find("/Canvas/nlu/regQA").GetComponent<Button>().interactable = false;

        //GameObject.Find("/Canvas/nlu/QAxml").GetComponent<InputField>().text = GameObject.Find("/Canvas/nlu/QAxml").GetComponent<InputField>().text.Replace("\r\n", "");
        StartCoroutine("SendXMLToServerQA", GameObject.Find("/Canvas/nlu/QAxml").GetComponent<InputField>().text);
    }

    IEnumerator SendTextToServerQA(string question)
    {
        WWWForm form = new WWWForm();
        form.AddField("tname", GameObject.Find("/Canvas/nlu/tname").GetComponent<InputField>().text);
        form.AddField("s", question);

        string prefx = "";
        if (!Application.isWebPlayer || Application.isEditor)
        {
            prefx = "www.l2f.inesc-id.pt/~pfialho/tts/";
        }

        WWW www = new WWW(prefx + "frameworkProxyQA.php", form);
        yield return www;
        if (www.error != null)
        {
            WWWerrorMsg = www.error;
            WWWerror = true;
            GameObject.Find("/Canvas/nlu/log").GetComponent<Text>().text = WWWerrorMsg;
            yield break;
        }
        GameObject.Find("/Canvas/nlu/answer").GetComponent<Text>().text = www.text;
        GameObject.Find("/Canvas/nlu/ask").GetComponent<Button>().interactable = true;
        GameObject.Find("/Canvas/nlu/regQA").GetComponent<Button>().interactable = true;
    }

    IEnumerator SendXMLToServerQA(string xml)
    {
        WWWForm form = new WWWForm();
        form.AddField("tname", GameObject.Find("/Canvas/nlu/tname").GetComponent<InputField>().text);
        form.AddField("xml", xml.Replace("\r\n", ""));

        string prefx = "";
        if (!Application.isWebPlayer || Application.isEditor)
        {
            prefx = "www.l2f.inesc-id.pt/~pfialho/tts/";
        }

        WWW www = new WWW(prefx + "frameworkProxyQA.php", form);
        yield return www;
        if (www.error != null)
        {
            WWWerrorMsg = www.error;
            WWWerror = true;
            GameObject.Find("/Canvas/nlu/log").GetComponent<Text>().text = WWWerrorMsg;
            yield break;
        }

        GameObject.Find("/Canvas/nlu/log").GetComponent<Text>().text = www.text;
    }

    public static String PrintXML(String XML)
    {
        String Result = "";

        MemoryStream mStream = new MemoryStream();
        XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode);
        XmlDocument document = new XmlDocument();

        try
        {
            // Load the XmlDocument with the XML.
            document.LoadXml(XML);

            writer.Formatting = Formatting.Indented;

            // Write the XML into a formatting XmlTextWriter
            document.WriteContentTo(writer);
            writer.Flush();
            mStream.Flush();

            // Have to rewind the MemoryStream in order to read
            // its contents.
            mStream.Position = 0;

            // Read MemoryStream contents into a StreamReader.
            StreamReader sReader = new StreamReader(mStream);

            // Extract the text from the StreamReader.
            String FormattedXML = sReader.ReadToEnd();

            Result = FormattedXML;
        }
        catch (XmlException)
        {
        }

        mStream.Close();
        writer.Close();

        return Result;
    }
}
