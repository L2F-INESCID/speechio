using System.Collections;
using System.Collections.Generic;

public class LangResources
{
    public static Dictionary<string, string> phonvisPT = new Dictionary<string, string>()
    {
        //{"#","#"},
        //{"@","@"},

        {"f","f"},
        {"v","f"},

        {"k","g"},
        {"g","g"},
        {"L","g"},
        {"J","g"},

        {"l","l"},
        {"l~","l"},
        {"R","l"},
        {"r","l"},

        {"O","O"},

        {"p","p"},
        {"b","p"},
        {"m","p"},

        {"s","s"},
        {"z","s"},

        {"t","t"},
        {"d","t"},
        {"n","t"},

        {"S","S"},
        {"Z","S"},

        {"a","a"},
        {"6","a"},
        {"6~","a"},
        {"6~j~","a"},
        {"6~w~","a"},

        {"e","e"},
        {"E","e"},
        {"e~","e"},
        
        {"i","i"},
        {"i~","i"},
        {"j","i"},
        {"j~","i"},

        {"o","o"},
        {"o~","o"},
        {"o~j~","o"},

        {"u","u"},
        {"u~","u"},
        {"w","u"},
        {"w~","u"},
        {"u~j~","u"}
    };

    public static Dictionary<string, string> phonvisEN = new Dictionary<string, string>()
    {
        {"p","p"},
        {"t","t"},
        //{"?",""},
        {"t^","t"},
        {"k","k"},
        {"x","x"},
        {"b","b"},
        {"d","d"},
        {"g","g"},
        {"ch","ch"},
        {"jh","j"},
        {"s","s"},
        {"z","z"},
        {"sh","sh"},
        {"zh","z"},
        {"f","f"},
        {"v","v"},
        {"th","th"},
        {"dh","d"},
        //{"h",""},
        {"m","m"},
        {"m!","m"},
        {"n","n"},
        {"n!","n"},
        {"ng","n"},
        {"l","l"},
        {"ll","l"},
        {"lw","l"},
        {"l!","l"},
        {"r","r"},
        {"y","y"},
        {"w","w"},
        {"hw","w"},
        {"e","e"},
        {"ao","o"},
        {"a","a"},
        {"ah","ah"},
        {"oa","o"},
        {"aa","o"},
        {"ar","a"},
        {"eh","eh"},
        {"oul","u"},
        {"ou","o"},
        {"ouw","o"},
        {"oou","o"},
        {"o","o"},
        {"au","a"},
        {"oo","o"},
        {"or","o"},
        {"our","o"},
        {"ii","i"},
        {"ihr","i"},
        {"iy","i"},
        {"i","i"},
        {"ie","i"},
        {"iii","i"},
        {"@r","r"},
        //{"@",""},
        {"uh","u"},
        {"uhr","u"},
        {"u","u"},
        {"uu","u"},
        {"iu","i"},
        {"uuu","u"},
        {"uw","u"},
        {"uul","u"},
        {"ei","e"},
        {"ee","e"},
        {"ai","a"},
        {"ae","a"},
        {"aer","a"},
        {"aai","a"},
        {"oi","o"},
        {"oir","o"},
        {"ow","o"},
        {"owr","o"},
        {"oow","o"},
        {"i@","i"},
        {"ir","i"},
        {"irr","i"},
        {"iir","i"},
        {"@@r","r"},
        {"er","e"},
        {"eir","e"},
        {"ur","u"},
        {"urr","u"},
        {"iur","i"}
    };

    public static Dictionary<string, string> phonvisES = new Dictionary<string, string>()
    {
        {"a","a"},
        {"e","e"},
        {"i","i"},
        {"o","o"},
        {"u","u"},
        {"i0","i"},
        {"u0","u"},
        {"a1","ah"},
        {"e1","eh"},
        {"i1","i"},
        {"o1","o"},
        {"u1","u"},
        {"p","p"},
        {"t","t"},
        {"k","k"},
        {"b","b"},
        {"d","d"},
        {"g","g"},
        {"f","f"},
        {"th","th"},
        {"s","s"},
        {"x","x"},
        {"ch","ch"},
        {"m","m"},
        {"n","n"},
        {"ny","n"},
        {"l","l"},
        {"ll","y"},
        {"r","r"},
        {"rr","r"}
    };

    public static string[] visPT = new string[] { 
        "a", "i", "p", "e", "f", "g", "l", "o", "s", "t", "u" };

    public static string[] visEN = new string[] { 
        "a","ah","b","ch","d","e","eh","f","g","i","j","k","l","m","n","o","p","r","s","sh","t","th","u","v","w","x","y","z" };  //"c",

    public static string[] visES = new string[] { 
        "a","ah","b","ch","d","e","eh","f","g","i","k","l","m","n","o","p","r","s","t","th","u","x","y" };      // "c","j"
}
