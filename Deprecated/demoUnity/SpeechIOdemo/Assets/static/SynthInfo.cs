using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SynthInfo
{
    public string sentence;
    public List<string> phon;
    public List<float> phontimes;
    public AudioClip audioOut;

    public SynthInfo(string s, List<string> p, List<float> pt, AudioClip a)
    {
        sentence = s;
        phon = p;
        phontimes = pt;
        audioOut = a;
    }
}
