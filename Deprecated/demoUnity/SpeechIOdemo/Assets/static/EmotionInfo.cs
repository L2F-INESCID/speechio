public class EmotionInfo
{
    public string text;
    public string emotion;
    public float duration;

    public EmotionInfo(string t, string e, float d)
    {
        text = t;
        emotion = e;
        duration = d;
    }
}
