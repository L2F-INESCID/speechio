// <copyright file="SpeechIO.cs" company="L2F/INESC-ID">
// Copyright (c) 2016 RAGE All rights reserved.
// </copyright>
// <author>Pedro Fialho and Wim</author>
// <date>07-Feb-17 15:30:11</date>
// <summary></summary>
namespace AssetPackage
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Linq;
    using System.Linq;
    using System.IO;
    using System.Text;
    using MiniJSON;
    using System.Collections;

    /// <summary>
    /// An asset.
    /// </summary>
    public class SpeechIO : BaseAsset
    {
        #region Fields

        /// <summary>
        /// Options for controlling the operation.
        /// </summary>
        private SpeechIOSettings settings = null;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the DemoAsset.Asset class.
        /// </summary>
        public SpeechIO()
            : base()
        {
            //! Create Settings and let it's BaseSettings class assign Defaultvalues where it can.
            // 
            settings = new SpeechIOSettings();
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets or sets options for controlling the operation.
        /// </summary>
        ///
        /// <remarks>   Besides the toXml() and fromXml() methods, we never use this property but use
        ///                it's correctly typed backing field 'settings' instead. </remarks>
        /// <remarks> This property should go into each asset having Settings of its own. </remarks>
        /// <remarks>   The actual class used should be derived from BaseAsset (and not directly from
        ///             ISetting). </remarks>
        ///
        /// <value>
        /// The settings.
        /// </value>
        public override ISettings Settings
        {
            get
            {
                return settings;
            }
            set
            {
                settings = (value as SpeechIOSettings);
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Retrieves a web page (https://www.google.nl/imghp).
        /// </summary>
        public void RetrieveWebPage()
        {
            //! The base Path is defined in the DemoAssetSettings
            //! The IWebServiceRequest interface is mainly targetted at REST webservices that have a base services path and a service specific part).
            // 
            RequestResponse response = IssueRequest(String.Empty, "GET");

            Log(Severity.Information, response.body);
        }


        private class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding { get { return Encoding.UTF8; } }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="langcode"></param>
        /// <param name="sbytes"></param>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public String SpeechToText(string langcode, byte[] sbytes, List<string> keywords)
        {
            string grxml = "";

            if (langcode.ToLower().Trim() == "fr")
            {
                grxml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><grammar root=\"main\" version=\"1.0\" xmlns=\"http://www.w3.org/2001/06/grammar\" xml:lang=\"fr-FR\"><rule id=\"main\"><item repeat=\"1-\"><one-of>";
                for (int i = 0; i < keywords.Count; i++)
                {
                    grxml += "<item>" + keywords[i] + "</item>";
                }

                grxml += "<item><ruleref uri=\"#garbage\"/></item></one-of></item></rule><rule id=\"garbage\"><item repeat=\"1-\"><item>background</item></item></rule></grammar>";
            }
            else
            {
                grxml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><grammar root=\"main\" version=\"1.0\" xmlns=\"http://www.w3.org/2001/06/grammar\" xml:lang=\"pt-PT\"><rule id=\"main\"><item repeat=\"0-1\"><one-of>";
                for (int i = 0; i < keywords.Count; i++)
                {
                    grxml += "<item><ruleref special=\"GARBAGE\"/>" + keywords[i] + "<ruleref special=\"GARBAGE\"/></item>";
                }

                grxml += "</one-of></item></rule></grammar>";
            }

            return SpeechToText(true, langcode, sbytes, grxml);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="kws"></param>
        /// <param name="lang"></param>
        /// <param name="sbytes"></param>
        /// <param name="grxml"></param>
        /// <returns></returns>
        public String SpeechToText(Boolean kws, string lang, byte[] sbytes, string grxml = "")
        {
            string langcode = lang.ToLower().Trim();

            if (langcode != "en" && langcode != "fr" && langcode != "pt")
            {
                Log(Severity.Error, "Language code '"+langcode+"' is not defined. Please use 'en', 'pt' or 'fr'");
                return "";
            }

            string formdata = "";
            formdata += "lang=" + langcode;

            
            if (kws)
            {
                if (String.IsNullOrEmpty(grxml))
                {
                    Log(Severity.Error, "Keyword spotting mode selected but the required XML grammar was not supplied. Please supply it (as a string) in parameter 'grxml'.");
                    return "";
                }
                
                // xml validations
                try
                {
                    using (var reader = XmlReader.Create(new StringReader(grxml)))
                    {
                        while (reader.Read()) { }
                    }
                }
                catch
                {
                    Log(Severity.Error, "Keyword spotting mode selected but the supplied grammar is not valid XML.");
                    return "";
                }

                XDocument doc = XDocument.Parse(grxml, LoadOptions.PreserveWhitespace);
                StringWriter writer = new Utf8StringWriter();
                doc.Save(writer, SaveOptions.DisableFormatting);

                Log(Severity.Verbose, "single line, no identation white space, xml grammar = " + grxml);

                formdata += "&task=echo-kws-asr&grxml=" + grxml;
            }
            else
            {
                formdata += "&task=echo-asr";
            }

            Byte[] audioinput = sbytes;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("[");
            int count = 0;
            for (int i = 0; i < audioinput.Length; i++)
            {
                if (i == audioinput.Length - 1)
                {
                    sb.Append(audioinput[i]);
                }
                else
                {
                    sb.Append(audioinput[i] + ",");
                }
                if (audioinput[i] != 0)
                    count++;
            }
            sb.Append("]");

            formdata += "&sbytes=" + sb.ToString();

            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/x-www-form-urlencoded");
            headers.Add("Accept", "text/plain");

            //settings.Host = "www.l2f.inesc-id.pt/~pfialho/test/echo2/frameworkProxy22.py";
            RequestResponse response = null;
            if (langcode == "fr")
            {
                //settings.Host = "www.l2f.inesc-id.pt/~pfialho/test/echo2/frameworkProxyFR.py";
                response = IssueRequest("/test/echo2/frameworkProxyFR.py", "POST", headers, formdata);
            }
            else
            {
                response = IssueRequest("/test/echo2/frameworkProxy22.py", "POST", headers, formdata);
            }                        
            
            string asrout = response.body;
            if (asrout.Trim() == "_REPEAT_" || (langcode == "fr" && asrout.Replace("background", "").Trim().Length == 0))
            {
                asrout = "";
            }
            else
            {
                if (langcode == "fr" && kws)
                {
                    asrout = asrout.Replace("background", " ");
                }
            }

            Log(Severity.Information, asrout);
            return asrout;
        }

        /// <summary>
        /// Get dialogue acts, for each sentence in a list. 
        /// The predicted dialogue act for each sentence depends on the remaining sentences in the list.
        /// This component is therefore intended for multiple sentences, although it works with a single sentence (ie, list length = 1). 
        /// </summary>
        /// <param name="sentences">List of sentences, from a dialogue or sequence</param>
        /// <returns>List of strings corresponding to the dialogue acts of each sentence in the input list (ie, length input list = length output list)</returns>
        public List<String> DialogueAct(List<String> sentences)
        {
            string tosend = "<root><text>";
            foreach (String sent in sentences)
            {
                tosend += "<s>" + sent + "</s>";
            }
            tosend += "</text></root>";
            tosend = tosend.Replace("<s></s>", "");

            Log(Severity.Verbose, tosend);

            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/xml");

            RequestResponse response = IssueRequest("/test/echo2/dialogActProxy.php", "POST", headers, tosend);
            string dacts = response.body;
            string xmlparse = dacts.Substring(dacts.IndexOf("<result>") + 8, dacts.IndexOf("</result>") - dacts.IndexOf("<result>") - 8);

            Log(Severity.Verbose, xmlparse);
            return new List<string>(xmlparse.Split('\n'));
        }

        public List<String> EmotionsInText(List<String> sentences)
        {
            string tosend = "<root><text>";
            foreach (String sent in sentences)
            {
                tosend += "<s>" + sent + "</s>";
            }
            tosend += "</text></root>";
            tosend = tosend.Replace("<s></s>", "");

            Log(Severity.Verbose, tosend);

            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/xml");

            RequestResponse response = IssueRequest("/test/echo2/dialogActProxy.php", "POST", headers, tosend);
            string dacts = response.body;
            string xmlparse = dacts.Substring(dacts.IndexOf("<result>") + 8, dacts.IndexOf("</result>") - dacts.IndexOf("<result>") - 8);

            Log(Severity.Verbose, xmlparse);
            return new List<string>(xmlparse.Split('\n'));
        }


        // TODO
        public List<TTSresult> TextToSpeech(string text)
        {
            return TextToSpeech(text, "OGG");
        }

        public List<TTSresult> TextToSpeech(string text, string codec = "OGG", string lang = "pt", string voice = "Vicente")
        {
            List<TTSresult> SynthResults = new List<TTSresult>();

            string langcode = lang.ToLower().Trim();
            string voiceid = voice; //.ToLower().Trim();
            string codecid = codec;

            if (langcode != "pt")
            {
                Log(Severity.Error, "Language code '" + langcode + "' is not defined. Please use 'pt'.");
                return SynthResults;
            }

            if (voiceid != "Vicente" && voiceid != "Viriato" && voiceid != "Violeta")
            {
                Log(Severity.Error, "Voice '" + voice + "' is not defined. Please use 'Vicente', 'Viriato' (M) or 'Violeta' (F).");
                return SynthResults;
            }

            if (codecid != "OGG" && codecid != "MP3")
            {
                Log(Severity.Error, "Codec '" + codec + "' is not defined. Please use 'OGG' or 'MP3' (case sensitive).");
                return SynthResults;
            }

            string formdata = "task=echo&lang=" + langcode + "&v=" + voiceid + "&platform=" + codecid + "&q=" + text;
            
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/x-www-form-urlencoded");
            headers.Add("Accept", "application/json");

            //settings.Host = "www.l2f.inesc-id.pt/~pfialho/tts/frameworkProxy.php";
            RequestResponse response = IssueRequest("/tts/frameworkProxy.php", "POST", headers, formdata);
            string jsontext = response.body;

            // parseJSONResponse
            Log(Severity.Verbose, "www: " + jsontext);

            var json2 = Json.Deserialize(jsontext) as Dictionary<string, object>;
            //Hashtable json2 = (Hashtable)MiniJSON.JsonDecode(jsontext);

            List<object> alltts = (List<object>)json2["ttsresults"];
            foreach (Dictionary<string, object> json in alltts)
            {
                List<string> phon = new List<string>();
                phon = new List<string>(((IEnumerable)((List<object>)json["phones"]).ToArray()).Cast<object>()
                                 .Select(x => x.ToString())
                                 .ToArray());

                //((List<object>)json["phones"]).ToArray()

                //lite invalid/empty result checking!...
                if (phon.Count <= 0)
                {
                    continue;
                }
                //print("phones: " + string.Join(",", SynthInfo.phon.ToArray(), 0, 5));
                
                //List<string> timestoprint = new List<string>();
                List<float> phontimes = new List<float>();
                List<object> tmp = (List<object>)json["times"];
                foreach (double f in tmp)
                {
                    phontimes.Add((float)f);
                    //timestoprint.Add(f.ToString());
                }
                //print("times: " + string.Join(",", timestoprint.ToArray(), 0, 5));

                string sentence = (string)json["sentence"];
                Log(Severity.Verbose, "sentence: " + sentence);
                
                string oggurl = "/test/echo2/" + (string)json["url"];
                Log(Severity.Verbose, "audio url: " + settings.Host + oggurl);

                //Dictionary<string, string> audioheaders = new Dictionary<string, string>();
                //audioheaders.Add("Content-Type", "application/octet-stream");
                //audioheaders.Add("Accept", "application/octet-stream");
                
                //RequestResponse audioresponse = IssueRequest(oggurl, "GET", audioheaders, "", true);
                //byte[] audioOut = audioresponse.binaryResponse;

                SynthResults.Add(new TTSresult(sentence, phon, phontimes, "http://" + settings.Host + oggurl));
            }

            Log(Severity.Information, "done");
            return SynthResults;
        }
        

        /// <summary>
        /// Issue a HTTP Webrequest.
        /// </summary>
        ///
        /// <param name="path">   Full pathname of the file. </param>
        /// <param name="method"> The method. </param>
        ///
        /// <returns>
        /// true if it succeeds, false if it fails.
        /// </returns>
        private RequestResponse IssueRequest(string path, string method)
        {
            Dictionary<string, string> headers = new Dictionary<string, string>();

            headers.Add("Content-Type", "application/json");
            headers.Add("Accept", "application/json");

            return IssueRequest(path, method, new Dictionary<string, string>(), String.Empty);
        }

        /// <summary>
        /// Issue a HTTP Webrequest.
        /// </summary>
        ///
        /// <param name="path">    Full pathname of the file. </param>
        /// <param name="method">  The method. </param>
        /// <param name="headers"> The headers. </param>
        /// <param name="body">    The body. </param>
        ///
        /// <returns>
        /// true if it succeeds, false if it fails.
        /// </returns>
        private RequestResponse IssueRequest(string path, string method, Dictionary<string, string> headers, string body = "", bool isbinary = false)
        {
            return IssueRequest(path, method, headers, body, settings.Port, isbinary);
        }
        
        /// <summary>
        /// Query if this object issue request 2.
        /// </summary>
        ///
        /// <param name="path">    Full pathname of the file. </param>
        /// <param name="method">  The method. </param>
        /// <param name="headers"> The headers. </param>
        /// <param name="body">    The body. </param>
        /// <param name="port">    The port. </param>
        ///
        /// <returns>
        /// true if it succeeds, false if it fails.
        /// </returns>
        private RequestResponse IssueRequest(string path, string method, Dictionary<string, string> headers, string body, Int32 port, bool isbinary=false)
        {
            IWebServiceRequest ds = getInterface<IWebServiceRequest>();

            RequestResponse response = new RequestResponse();

            if (ds != null)
            {
                Uri uri = new Uri(string.Format("http{0}://{1}{2}{3}/{4}",
                                   settings.Secure ? "s" : String.Empty,
                                   settings.Host,
                                   port == 80 ? String.Empty : String.Format(":{0}", port),
                                   String.IsNullOrEmpty(settings.Path.TrimEnd('/')) ? "" : settings.Path.TrimEnd('/'),
                                   path.TrimStart('/')
                                   ).TrimEnd('/'));
                Log(Severity.Verbose, uri.ToString());
                //Log(Severity.Verbose, body);

                ds.WebServiceRequest(
                          new RequestSetttings()
                          {
                              method = method,
                              uri = uri,
                              requestHeaders = headers,
                              //! allowedResponsCodes,     // TODO default is ok
                              body = body, // or method.Equals("GET")?string.Empty:body
                              hasBinaryResponse = isbinary
                          }, out response);
            }

            return response;
        }
        
        //private RequestResponse IssueRequest(string path, string method, Dictionary<string, string> headers, Dictionary<string, string> parameters, Int32 port)
        //            {
        //    String body  =
        //        return IssueRequest(path, method, header, body, port);
        //}

        #endregion Methods
    }
}