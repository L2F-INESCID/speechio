﻿namespace AssetPackage
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class TTSresult
    {
        public int samplerate { get { return 16000; } }
        public int numchannels { get { return 1; } }
        public int bitspersample { get { return 16; } }

        public string sentence { get; }
        public List<string> phonemes { get; }
        public List<float> phondurations { get; }
        public string audioURL { get; }

        public TTSresult(string sentence, List<string> phonemes, List<float> phondurations, string audioURL)
        {
            this.sentence = sentence;
            this.phonemes = phonemes;
            this.phondurations = phondurations;
            this.audioURL = audioURL;
        }
    }
}
