namespace AssetPackage
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    /// <summary>
    /// An asset settings.
    /// 
    /// BaseSettings contains the (de-)serialization methods.
    /// </summary>
    public class SpeechIOSettings : BaseSettings
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the DemoAsset.AssetSettings class.
        /// </summary>
        public SpeechIOSettings()
            : base()
        {
            // Set Default values here.
            Host = "www.l2f.inesc-id.pt/~pfialho";
            Port = 80;
            Path = "/";
            Secure = false;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets or sets the host.
        /// </summary>
        ///
        /// <value>
        /// The host.
        /// </value>
        public String Host
        {
            get;
            set;
        }

        /// Gets or sets the GameStorage Service port.
        /// </summary>
        ///
        /// <value>
        /// The host.
        /// </value>
        public int Port
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether to use http or https.
        /// </summary>
        ///
        /// <value>
        /// true if secure, false if not.
        /// </value>
        public bool Secure
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the full pathname of the url.
        /// </summary>
        ///
        /// <value>
        /// The full pathname of the file.
        /// </value>
        public String Path
        {
            get;
            set;
        }

        #endregion Properties
    }
}
