﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AssetManagerPackage;
using AssetPackage;
using GameCode;
using System.Collections.Generic;

namespace SpeechIO_Tests
{
    [TestClass]
    public class UnitTestsSPA
    {
        [TestMethod]
        public void TestsSPA()
        {
            AssetManager.Instance.Bridge = new Bridge();

            SpeechIO asset = new SpeechIO();

            // sentence order and entailment affects final result
            List<String> dacts = asset.DialogueAct(new List<string> { "Hello!", "Hi, how are you?", "I'm awesome!",
                "Do you think I am amazing?", "No.", "Why do you say that?", "Because I am the best!",
                "Okay, sure." });
            
        }
    }
}
