﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using AssetManagerPackage;
using AssetPackage;
using GameCode;
using System.Collections.Generic;
using System.IO;

namespace SpeechIO_Tests
{
    [TestClass]
    public class UnitTestTTS
    {
        [TestMethod]
        public void TestTTS()
        {
            AssetManager.Instance.Bridge = new Bridge();

            SpeechIO asset = new SpeechIO();

            //! These defaults are alrady set in the *AssetSettings constructor.
            //(asset.Settings as SpeechIOSettings).Host = "google.pt";
            //(asset.Settings as SpeechIOSettings).Port = 80;
            //(asset.Settings as SpeechIOSettings).Path = "/imghp";

            //asset.RetrieveWebPage();
            List<TTSresult> SynthResults = asset.TextToSpeech("bom dia");

            //BinaryWriter Writer = null;
            //string Name = @"E:\istoeumteste.ogg";
            
            // Create a new stream to write to the file
            //Writer = new BinaryWriter(File.OpenWrite(Name));

            //Dictionary<string, string> audioheaders = new Dictionary<string, string>();
            //audioheaders.Add("Content-Type", "application/octet-stream");
            //audioheaders.Add("Accept", "application/octet-stream");

            //RequestResponse audioresponse = IssueRequest(oggurl, "GET", audioheaders, "", true);
            //byte[] audioOut = audioresponse.binaryResponse;

            //// Writer raw data                
            //Writer.Write(SynthResults[0].rawaudio);
            //Writer.Flush();
            //Writer.Close();


            SynthResults = asset.TextToSpeech("bom dia", "MP3");

            //Writer = null;
            //Name = @"E:\istoeumteste.mp3";

            //// Create a new stream to write to the file
            //Writer = new BinaryWriter(File.OpenWrite(Name));

            //// Writer raw data                
            //Writer.Write(SynthResults[0].rawaudio);
            //Writer.Flush();
            //Writer.Close();
        }
    }
}
