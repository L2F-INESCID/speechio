﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using AssetManagerPackage;
using AssetPackage;
using GameCode;
using System.Collections.Generic;

namespace SpeechIO_Tests
{
    [TestClass]
    public class UnitTestASR
    {
        [TestMethod]
        public void TestASR()
        {
            AssetManager.Instance.Bridge = new Bridge();

            SpeechIO asset = new SpeechIO();

            // all inputs must be raw audio input (ie, without header/metadata) at 16kHz, 16bit, mono

            // FR
            byte[] bytesFR = System.IO.File.ReadAllBytes(@"e:\testFR.raw");
            asset.SpeechToText(false, "fr", bytesFR);
            asset.SpeechToText("fr", bytesFR, new List<string> { "je", "Pierre" });

            // PT
            byte[] bytesPT = System.IO.File.ReadAllBytes(@"e:\testPT.raw");
            asset.SpeechToText(false, "pt", bytesPT);
            asset.SpeechToText("pt", bytesPT, new List<string> { "chama", "porque" });
        }
    }
}
